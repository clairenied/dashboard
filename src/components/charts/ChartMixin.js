import Chart from 'chart.js/dist/Chart';
import ChartUtils from './ChartUtils';

const ChartMixin = {
  props: {
    datasets: {
      type: Array,
      default() {
        return [12, 19, 3, 5, 2, 3];
      },
    },
    legendLabel: {
      type: String,
      default() {
        return '';
      },
    },
    stacked: {
      type: Boolean,
    },
    theme: {
      type: Object,
      default() {
        return {};
      },
    },
    labels: {
      type: Array,
      default() {
        return ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'];
      },
    },
    title: {
      type: String,
      default: '',
    },
  },
  data() {
    return {
      chart: Object.create(null),
      config: {
        type: 'bar',
        data: ChartUtils.normalizeData(this),
        options: {
          maintainAspectRatio: false,
          responsive: true,
          scales: {
            xAxes: [],
            yAxes: [{
              ticks: {
                beginAtZero: true,
              },
            }],
          },
        },
      },
    };
  },
  methods: {
    init() {
      const ctx = this.$refs.chart.getContext('2d');
      ctx.height = 500;
      if (this.title !== '') {
        this.config.options.title = {
          text: this.title,
          display: true,
        };
      }
      if (this.stacked) {
        this.config.options.scales.xAxes.push({
          stacked: true,
        });
        this.config.options.scales.yAxes[0].stacked = true;
      }
      if (!this.legendLabel && ChartUtils.isArrayOfNumbers(this.datasets)) {
        this.config.options.legend = {
          display: false,
        };
      }
      this.chart = new Chart(ctx, this.config);
    },
  },
};

export default ChartMixin;
