const ChartUtils = {
  backgroundColor: [
    'rgba(255, 99, 132, 0.2)',
    'rgba(54, 162, 235, 0.2)',
    'rgba(255, 206, 86, 0.2)',
    'rgba(75, 192, 192, 0.2)',
    'rgba(153, 102, 255, 0.2)',
    'rgba(255, 159, 64, 0.2)',
  ],
  borderColor: [
    'rgba(255, 99, 132, 1)',
    'rgba(54, 162, 235, 1)',
    'rgba(255, 206, 86, 1)',
    'rgba(75, 192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64, 1)',
  ],
  borderWidth: 3,
  getDataType(data) {
    return Object.prototype.toString.call(data);
  },
  isArray(datasets) {
    const datasetType = this.getDataType(datasets);
    return datasetType === '[object Array]';
  },
  isArrayOfNumbers(datasets) {
    return this.isArray(datasets) && this.getDataType(datasets[0]) === '[object Number]';
  },
  isArrayOfObjects(datasets) {
    return this.isArray(datasets) && this.getDataType(datasets[0]) === '[object Object]';
  },
  normalizeData(dataObj) {
    const { labels, legendLabel, theme } = dataObj;
    let { datasets } = dataObj;
    // is the dataset an array or object?
    if (this.isArrayOfNumbers(datasets)) {
      datasets = [{
        backgroundColor: theme.backgroundColor || this.backgroundColor,
        borderColor: theme.borderColor || this.borderColor,
        borderWidth: theme.borderWidth || this.borderWidth,
        data: datasets,
      }];
      if (legendLabel) {
        datasets[0].label = legendLabel;
      }
    } else if (this.isArrayOfObjects(datasets)) {
      datasets = datasets.map((dataset, i) => {
        const thisDataset = dataset;
        thisDataset.backgroundColor = thisDataset.backgroundColor
          || theme.backgroundColor
          || this.backgroundColor[i];
        thisDataset.borderColor = thisDataset.borderColor
          || theme.borderColor
          || this.borderColor[i];
        thisDataset.label = thisDataset.label
          || theme.label
          || this.label;
        thisDataset.borderWidth = thisDataset.borderWidth
          || theme.borderWidth
          || this.borderWidth;
        return thisDataset;
      });
    }
    const updatedData = {
      labels,
      datasets,
    };
    return updatedData;
  },
};

export default ChartUtils;
